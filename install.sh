#! /bin/bash
# Description : This script will install Ansible, Vagrant and Virtualbox for the Apiculteur project
# Last modification : 30/03/2022

# Sources : 
# https://www.virtualbox.org/wiki/Downloads
# https://www.vagrantup.com/downloads
# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

error() {
  printf '\E[31m'; echo "$@"; printf '\E[0m'
}

info() {
  printf '\E[34m'; echo "$@"; printf '\E[0m'
}

if [[ $EUID > 0 ]]; then
    error "This script should be run using sudo or as the root user"
    exit 1
fi

function i_utils {
  echo
  info "Installation of wget, gnupg, software-properties-common curl python3 and python3-pip"
  echo
  apt install -y wget gnupg software-properties-common curl python3 python3-pip
}

function i_ansible {
  echo
  info "Installation of Ansible"
  echo
  echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu focal main" | tee /etc/apt/sources.list.d/ansible.list
  apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
}

function i_vagrant {
  echo
  info "Installation of Vagrant"
  echo
  echo "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
  curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
}

function i_virtualbox {
  echo
  info "Installation of Virtualbox"
  echo
  echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" | tee /etc/apt/sources.list.d/virtualbox.list
  wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -
}

function yes_or_no {
  while true; do
    read -p "$* [Y/n]: " yn
      case $yn in
        [YyOo]* | "") return 0 ;;
        [Nn]*) error "Aborted" ; exit 1 ;;
      esac
  done
}

echo
info "This script will install Ansible, Vagrant and Virtualbox for the Apiculteur project"
echo

yes_or_no "Are you ready to proceed ?"

i_utils
i_ansible
i_vagrant
i_virtualbox

apt update
apt install -y ansible vagrant virtualbox-6.1
pip install -r ./requirements.txt

echo
info "Apiculteur's requirements installed"
echo
info "To use Apiculteur :

cd Apiculteur/src
python3 main.py --help"
echo

exit 0
