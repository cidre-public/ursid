from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

@login_required()
def home(request):
    return redirect('/polls')