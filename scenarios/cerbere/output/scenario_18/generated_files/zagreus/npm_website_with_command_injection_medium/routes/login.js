var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('login', {});
});


router.post('/', function(req, res, next) {
    const users = req.app.get('users');
    let logged = false;
    let i = 0;
    do {
        const u = users[i];
        if (req.body.login == u.login && req.body.password == u.password) {
            req.session.user = u;
            logged = true;
        }
    } while (!logged && ++i < users.length);
    if (logged) {
        res.redirect('/');
    } else {
        res.render('login', { wrong_creds: true })
    }
});

module.exports = router;
