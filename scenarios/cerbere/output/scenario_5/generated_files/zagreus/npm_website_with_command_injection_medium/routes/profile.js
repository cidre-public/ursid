const { exec } = require('child_process');
var express = require('express');
var router = express.Router();

router.get('/:login', function(req, res, next) {
    let users, user;
    if (process.env.BROKEN_ACCESS == "true") {
        users = req.app.get('users');
        user = users.find(el => el.login == req.params.login);
    } else {
        user = req.session.user;
    }
    const images = req.app.get('images');
    const u_images = images.filter(el => el.user == user.login);
    res.render('profile', {user, images: u_images});
});


function execute_search(q) {
    
}


router.get('/:login/search', function(req, res, next) {
    let users, user;
    if (process.env.BROKEN_ACCESS == "true") {
        users = req.app.get('users');
        user = users.find(el => el.login == req.params.login);
    } else {
        user = req.session.user;
    }
    let q = req.query.q;
    if (process.env.CMD_INJECTION != "easy") {
        if (q.length > 25) {
            next({status: 400, message: "Query too long"});
        }
    }
    if (process.env.CMD_INJECTION == "hard") {
        let blacklist = [';', '|', '$', '>', '<'];
        if (blacklist.some(c => q.includes(c))) {
            next({status: 400, message: "Unauthorized character used"});
        } else if (q.includes('&') && !q.includes('&&')) {
            next({status: 400, message: "Unauthorized character used"});
        }
    }
    exec(`find public/images/ -type f -name '*.jpg' | grep -i ${q}`, (error, stdout, stderr) => {
        if (error) {
            console.log("Error, could not get user's list of images");
            res.render('profile', {user, images: []});
        } else {
            let output = stdout.trim().split('\n');
            output.forEach((el, i, o) => {
                if (el.startsWith('public/images/')) {
                    let new_el = el.substring(14);
                    new_el = new_el.split('/');
                    if (new_el.length > 2) {
                        o[i] = {name: new_el[2], user: new_el[0], premium: true};
                    } else {
                        o[i] = {name: new_el[1], user: new_el[0], premium: false};
                    }
                } else {
                    o[i] = {name: el, user: user.login, premium: false};
                }
            });
            res.render('profile', {user, images: output});
        }
    });
});


router.post('/modify-user', function(req, res, next) {
    const users = req.app.get('users');
    const u_index = users.findIndex(el => el.login == req.body.login && el.password == req.body.password);
    if (u_index != -1) {
        users[u_index].login = req.body.login;
        users[u_index].email = req.body.email;
        res.redirect(`/profile/${req.body.login}`);
    } else {
        res.status(400);
        res.render('profile', {user: req.session.user});
    }
});

module.exports = router;
