### WORK IN PROGRESS

A very simple web scenario in order to test network and website issues.

####Contains
- 3 machines, named webservice0, webservice1 and webservice2
- All those machines have a simple apache webserver running a single page index.php.
- This page displays the IP, machine name and user name responsible for the page.

####How to use
In directory src:

```python3 main.py ../scenarios/web_poc/scenario.json```

```cd output && vagrant up```

You may then access the websites through your browser.

```192.168.56.2/index.php ```

```192.168.56.3/index.php ```

```192.168.56.4/index.php ```

#### Known issues
- Launching this scenario then launching the poc scenario might result in the vagrant machines not being destroyed properly. Be sure to run vagrant destroy in this folder or to delete the box manually in virtualbox if you are planning to run the other scenario next.
#### TODO:
DNS server to redirect IPs?
