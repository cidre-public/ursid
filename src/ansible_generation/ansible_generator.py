import random, string, crypt
import logging
import os, os.path

from parsing.VM import VirtualMachine
from utils.static import static
import utils.text as text
import ansible_generation.interface_vagrant as int_vagrant
import re

log = logging.getLogger(__name__)
INSTALL_SCRIPT_PATH_FROM_OUTPUT = "../../ansible_generation/install_scripts"
INSTALL_SCRIPT_PATH_FROM_MAIN = "./ansible_generation/install_scripts"
INSTALL_SERVICE_PATH_FROM_MAIN = "./ansible_generation/service_scripts"

# TODO: NPM service script should not depend on folder name


def gen_password_hash(clear_password: str) -> str:
    randomsalt = "".join(random.sample(string.ascii_letters, 8))
    return crypt.crypt(clear_password, f"$6${randomsalt}$")


def service_task(user, service):
    """Adds a create_service task in the playbook.
    This will be called by user_task if the ["Service"] key in the user is not empty.
    This first adds a script to be executed by the service, then the service file to the proper directory,
    then calls ansible builtin service to run it and enable it on reboot.
    """

    # path to the file to the script that'll be launched by the service
    if "Services" not in user:
        return ""
    script_path_from_main = f"""{INSTALL_SERVICE_PATH_FROM_MAIN}/{service}_service.sh"""
    # now we load that script, edit some of it user specific values and write the result
    # result is written to a
    with open(script_path_from_main, "r") as inFile:
        script = inFile.read()
    # gets a new non existing file name to store the result
    i = 1
    if not os.path.isdir(f"""{INSTALL_SERVICE_PATH_FROM_MAIN}/generated/"""):
        os.mkdir(f"""{INSTALL_SERVICE_PATH_FROM_MAIN}/generated/""")
    new_script_name = f"""{INSTALL_SERVICE_PATH_FROM_MAIN}/generated/{service}_service.sh"""
    while os.path.exists(new_script_name):
        i += 1
        new_script_name = f"""{INSTALL_SERVICE_PATH_FROM_MAIN}/generated/{service}_service{str(i)}.sh"""
    with open(new_script_name, "w+") as outFile:
        outFile.write(script.format(username=user["Name"]))

    # Now the same thing for the service file
    service_path_from_main = f"""{INSTALL_SERVICE_PATH_FROM_MAIN}/{service}_service.service"""
    with open(service_path_from_main, "r") as inFile:
        script = inFile.read()
    i = 1
    new_service_name = f"""{INSTALL_SERVICE_PATH_FROM_MAIN}/generated/{service}_service.service"""
    while os.path.exists(new_service_name):
        i += 1
        new_service_name = f"""{INSTALL_SERVICE_PATH_FROM_MAIN}/generated/{service}_service{str(i)}.service"""
    script_name_without_number_and_extension = os.path.splitext(re.search("[^\d]*", new_script_name).group())[0]
    script_name_without_number = script_name_without_number_and_extension + ".sh"
    with open(new_service_name, "w+") as outFile:
        outFile.write(script.format(username=user["Name"], usergroup=user["Group"],
                                    script_path=os.path.basename(script_name_without_number)))
    service_name_without_number_and_extension = os.path.splitext(re.search("[^\d]*", new_service_name).group())[0]
    service_name_without_number = service_name_without_number_and_extension + ".service"
    return f"""- name: Adding script to execute {script_path_from_main} to /usr/bin
  become: yes
  ansible.builtin.copy:
    src: ../../{new_script_name}
    dest: "/usr/bin/{os.path.basename(script_name_without_number)}"
    owner: {user["Name"]}
    group: {user["Group"]}
    mode: 0755
  
- name: Adding service file to /etc/systemd/system to be executed as user {user["Name"]}
  become: yes
  ansible.builtin.copy:
    src: ../../{new_service_name}
    dest: "/etc/systemd/system/{os.path.basename(service_name_without_number)}"
    owner: root
    group: root
    mode: 0644
    
- name: Launch service and enable it at boot
  become: true
  ansible.builtin.service:
    name: {os.path.splitext(os.path.basename(service_name_without_number))[0]}
    state: restarted
    enabled: yes
    
"""


def user_task(user) -> str:
    """Adds a create_user task in the playbook
    """

    return_string = f"""- name: Add user {user["Name"]}
  become: yes
  ansible.builtin.user:
    name: "{user["Name"]}"
    password: "{gen_password_hash(user["Credentials"]["Value"])}"
    shell: "/bin/bash"
    create_home: yes

"""
    if user["Privilege"] == "SuperUser":
        return_string += f"""- name: Adding user {user["Name"]} to sudoers
  become: yes
  ansible.builtin.lineinfile:
    dest: "/etc/sudoers"
    line: "{user["Name"]} ALL=(ALL) NOPASSWD: ALL"
    insertafter: "EOF"

"""

    return return_string


def append_file_task(file: object) -> str:
    with open(file["Source"]) as inFile:
        line_to_append = inFile.read()
    return f"""- name: Append {file["Source"].split("/")[-1]} file to {file["Destination"]} 
  become: yes
  ansible.builtin.lineinfile:
    dest: "{file["Destination"]}"
    line: "{line_to_append}"
    insertafter: "EOF"

"""


def appendplus_file_task(file: object) -> str:
    with open(file["Source"]) as inFile:
        line_to_append = inFile.read()
    return f"""- name: Make sure {os.path.dirname(file["Destination"])} directory exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    state: directory
    

- name: Append {file["Source"].split("/")[-1]} file to {file["Destination"]} 
  become: yes
  ansible.builtin.lineinfile:
    dest: "{file["Destination"]}"
    line: "{line_to_append}"
    insertafter: "EOF"
    create: yes

"""


def edit_file_task(file: object) -> str:
    with open(file["Source"]) as inFile:
        line_to_edit = inFile.read()
    line_to_find = file["Modification"].split()[1]
    print(line_to_find)
    return f"""- name: Edit line containing {line_to_find} of {file["Destination"]} with {file["Source"].split("/")[-1]}
  become: yes
  ansible.builtin.lineinfile:
    dest: "{file["Destination"]}"
    search_string: "{line_to_find}"
    line: "{line_to_edit}"

"""


def editplus_file_task(file: object) -> str:
    with open(file["Source"]) as inFile:
        line_to_edit = inFile.read()
    line_to_find = file["Modification"].split()[1]

    return f"""- name: Make sure {os.path.dirname(file["Destination"])} directory exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    state: directory
    
    
- name: Edit line containing {line_to_find} of {file["Destination"]} with {file["Source"].split("/")[-1]}
  become: yes
  ansible.builtin.lineinfile:
    dest: "{file["Destination"]}"
    create: yes
    search_string: "{line_to_find}"
    line: "{line_to_edit}"
    insertafter: "EOF"

"""


def copy_file_task(file: object) -> str:
    """Adds a copy_file task in the playbook.

    Args:
        file (object): Object expected to the following format :

        {
            "Destination": "/root/file1",
            "Source": "/full/path/to/file1",
            "Owner": "root",
            "Group": "root",
            "Permissions": "0640",
            "before_packages": True
        }
"""
    return f"""- name: Make sure {os.path.dirname(file["Destination"])} directory exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    state: directory

- name: Copy {file["Source"].split("/")[-1]} file to {file["Destination"]} 
  become: yes
  ansible.builtin.copy:
    src: "{file["Source"]}"
    dest: "{file["Destination"]}"
    owner: {file["Owner"]}
    group: {file["Group"]}
    mode: {file["Permissions"]}

"""


def copy_directory_task(file: object) -> str:
    """Adds a copy_directory task in the playbook
    We can't use ansible.copy because it's so slow on directories, so we use synchronize.
    But synchronize can't set permissions so we have to make that a separate task (fun).

    Args:
        file (object): Object expected to the following format :

        {
            "Destination": "/root/file1",
            "Source": "/full/path/to/file1",
            "Owner": "root",
            "Group": "root",
            "Permissions": "0640",
            "before_packages": True
        }
"""
    return f"""- name: Make sure {os.path.dirname(file["Destination"])} directory exists
  become: yes
  file:
    path: "{os.path.dirname(file["Destination"])}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    state: directory

- name: Copy {file["Source"].split("/")[-1]} directory to {file["Destination"]} 
  become: yes
  ansible.posix.synchronize:
    dest: "{file["Destination"]}"
    src: "{file["Source"]}"

- name: Change {file["Source"].split("/")[-1]} permissions
  become: yes
  ansible.builtin.file:
    recurse: yes
    path: "{file["Destination"]}"
    owner: "{file["Owner"]}"
    group: "{file["Group"]}"
    mode: "{file["Permissions"]}"

"""


def add_repo_task(repo: object) -> str:
    """Adds a add_repo task in the playbook

    Args:
        repository (object): Object expected to the following format :

        {
            "repo": "deb http://archive.canonical.com/ubuntu hardy partner"
        }
    """
    return f"""- name: Add specified repository into sources list
  become: yes
  ansible.builtin.apt_repository:
    repo: {repo["repo"]}
    state: present

"""


def add_key_task(key: object) -> str:
    """Adds a add_key task in the playbook

    Args:
        key (object): Object expected to the following format :

        {
            "key_id": "36A1D7869245C8950F966E92D8576A8BA88D21E9"
            "key_server": "keyserver.ubuntu.com"
        }
    """
    return f"""- name: Add an apt key by id from {key["key_server"]}
  become: yes
  ansible.builtin.apt_key:
    keyserver: {key["key_server"]}
    id: {key["key_id"]}

"""


def aux_install_package(package):
    """ Specific installation instructions for some packages.
    Args:
        package: a package.
    """
    if not os.path.isdir(f"""{INSTALL_SCRIPT_PATH_FROM_MAIN}/generated/"""):
        os.mkdir(f"""{INSTALL_SCRIPT_PATH_FROM_MAIN}/generated/""")
    output_script_path = f"""{INSTALL_SCRIPT_PATH_FROM_OUTPUT}/generated/install_{package["name"]}_{package["version"]}.sh"""
    script_path_from_main = f"""{INSTALL_SCRIPT_PATH_FROM_MAIN}/generated/install_{package["name"]}_{package["version"]}.sh"""
    # write a new install_vulnerable_sudo_file unless it already exists:
    if not os.path.exists(script_path_from_main):
        with open(f"""{INSTALL_SCRIPT_PATH_FROM_MAIN}/install_{package["name"]}.sh""", "r") as inFile:
            script = inFile.read()
        with open(script_path_from_main, "w+") as outFile:
            outFile.write(script.format(package_version=package["version"]))

    return f"""- name: Install {package["name"]} at version {package["version"]}
  become: yes
  ansible.builtin.script:
    cmd: {output_script_path}

"""


def install_package_task(package: object) -> str:
    """Adds a install_package task in the playbook

    Args:
        package (object): Object expected to the following format :

        {
            "name": "foo"
            "version": "latest"
        }
    """
    # TODO: more elegant way of handling file path
    SUPPORTED_EDGE_CASES = ["sudo", "npm", "python"]
    if package["name"] in SUPPORTED_EDGE_CASES:
        return aux_install_package(package)

    if package["version"] == "latest" or package["version"] == "*":
        return f"""- name: Install package {package["name"]}
  become: yes
  apt: 
    name: {package["name"]}
    allow_unauthenticated : yes
    update_cache: yes

"""
    if package["version"] == "deb":
        return f"""- name: Install debian package {package["name"].split("/")[-1]}
  become: yes
  apt:
    deb: {package["name"]}
    allow_unauthenticated : yes

"""
    else:
        return f"""- name: Install version {package["version"]} of package {package["name"]}
  become: yes
  apt: 
    name: {package["name"]}={package["version"]}
    update_cache: yes
    allow_unauthenticated : yes

"""


def install_package_post_task(package: object) -> str:
    """Adds post tasks needed to install a package properly (usually inline commands)

    Args:
        package (object): Object expected to the following format :

        {
            "name": "foo"
            "version": "latest"
        }
    """
    SUPPORTED_EDGE_CASES = ["auditd"]
    if package["name"] not in SUPPORTED_EDGE_CASES:
        return ""
    else:
        match package["name"]:
            case "auditd":
                # idk why this doesn't work automatically
                # redirect to /dev/null to not trigger ansible errors if sth in the rules is not relevant
                force_load = f"""- name: Force auditd load because it doesn't work otherwise
  become: true
  ansible.builtin.command: 'auditctl -R /etc/audit/rules.d/audit.rules'
  ignore_errors: true
  
"""
                return force_load


def inline_files_task(file):
    """Takes a file object and uses its information to execute a script from the correct location with the correct
    privileges on the resulting machine.
      Args:output
        file (object): Object expected to the following format :

        {
            "Destination": "/root/file1",
            "Source": "/full/path/to/file1",
            "Owner": "root",
            "Group": "root",
            "Permissions": "0640",
            "before_packages": True
        }
    """

    return f"""- name: Copy script {file["Source"]} at destination {file["Destination"]} with execution privileges.
  become: true
  ansible.builtin.copy:
    src: {file["Source"]}
    dest: {file["Destination"]}
    mode: '0777'
 
- name: Execute script {file["Destination"]} as user {file["Owner"]}
  become: true
  become_user: {file["Owner"]}
  ansible.builtin.command:
    cmd: ./{os.path.basename(file["Destination"])}
    chdir: {os.path.dirname(file["Destination"])}

- name: delete installation script {file["Destination"]}
  become: true
  ansible.builtin.file:
    path: {file["Destination"]}
    state: absent
    
"""


def inline_task(inline: str, user: str = None) -> str:
    """Ansible task to run inline command

    Args:
        inline (str): command

    Returns:
        str: ansible task
    """
    # The magic number MAX_INLINE_LENGTH is used to shorten the task's written name
    MAX_INLINE_LENGTH = 20
    if user:
        return f"""- name: Execute inline command "{inline if len(inline) <= MAX_INLINE_LENGTH else f"{inline[:MAX_INLINE_LENGTH]}..."}"
          become: yes
          become_user: {user}
          ansible.builtin.shell:
            cmd: "{inline}"

        """
    else:
        return f"""- name: Execute inline command "{inline if len(inline) <= MAX_INLINE_LENGTH else f"{inline[:MAX_INLINE_LENGTH]}..."}"
  become: yes
  ansible.builtin.shell:
    cmd: "{inline}"

"""


def file_task(file):
    # checks the type of file task
    task_type = file["Modification"].split()[0]
    # checks whether this is a file or directory
    is_directory = os.path.isdir(file["Source"])

    if is_directory:
        return copy_directory_task(file)

    match task_type:
        case "APPEND":
            return append_file_task(file)
        case "APPEND+":
            return appendplus_file_task(file)
        case "WRITE":
            return copy_file_task(file)
        case "EDIT+":
            return editplus_file_task(file)
        case "EDIT":
            return edit_file_task(file)
        case _:
            raise ValueError("Error: Task type not recognized.")


def restart_service_task(service_name: str) -> str:
    return f"""- name: Restart "{service_name}" service
  become: yes
  ansible.builtin.service:
    name: {service_name}
    state: restarted
    enabled: yes

"""


def uninstall_ubuntu_advantage():
    return f"""- name: Uninstall ubuntu advantage
  become: yes
  ansible.builtin.apt:
    name: ubuntu-advantage-tools
    state: absent

"""


def generate_main_playbook(vm: VirtualMachine, dest_dir: str):
    log.debug(vm.name + " playbook generated")
    dest_path = dest_dir + f"/{int_vagrant.get_vm_playbook_name(vm)}"
    main_playbook = open(dest_path, "w")
    main_playbook.write(
        f"""---
- name: Main playbook
  {"" if static["args"].ansible_ignore_errors else "# "}ignore_errors: yes
  hosts: all
  vars:
  tasks:
"""
    )

    # Do OS specific actions (for now only ubuntu)
    if vm.get_image().find("ubuntu") != -1:
        # need to disable the ubuntu paid service
        # otherwise packages try to install from their private repository
        # it's very dumb
        main_playbook.write(text.indent_str(uninstall_ubuntu_advantage()))

    # Add users
    for user in vm.get_users():
        main_playbook.write(text.indent_str(user_task(user)))

    # Add gpg package if keys need to be add (necessary to add repository keys)
    if len(vm.get_keys()) > 0:
        main_playbook.write(
            text.indent_str(install_package_task({"name": "gpg", "version": "latest"}))
        )

    # Add repositories keys
    for key in vm.get_keys():
        main_playbook.write(text.indent_str(add_key_task(key)))

    # Add repositories
    for repo in vm.get_repos():
        main_playbook.write(text.indent_str(add_repo_task(repo)))

    # Copy files before packages installation
    # Example : .deb
    for file in vm.get_files():
        if file["before_packages"]:
            main_playbook.write(text.indent_str(file_task(file)))

    # Packages installation
    for package in vm.get_packages():
        main_playbook.write(text.indent_str(install_package_task(package)))

    # Copy files after packages installation
    # Example : conf file mariadb.conf
    for file in vm.get_files():
        if not file["before_packages"]:
            main_playbook.write(text.indent_str(file_task(file)))

    # Add inline commands
    for inline in vm.get_inline_commands():
        main_playbook.write(text.indent_str(inline_task(inline)))

    for file in vm.get_inline_file_commands():
        main_playbook.write(text.indent_str(inline_files_task(file)))

    # Switch to post_tasks
    main_playbook.write(f"""
  post_tasks:
""")


    # Add services attached to users
    for user in vm.get_users():
        if "Services" in user:
            for service in user["Services"]:
                main_playbook.write(text.indent_str(service_task(user, service)))

    # Add services restart
    # not sure what this does, student implem not really used anymore
    for service in vm.get_services_names():
        main_playbook.write(text.indent_str(restart_service_task(service)))

        # Run any post_task install commands linked to packages
    for package in vm.get_packages():
        main_playbook.write(text.indent_str(install_package_post_task(package)))


def generate_all_main_playbooks(vms: "list[VirtualMachine]", dest_dir: str):
    """
    Generate a main_playbook.yml from a list of virtual machine objects

    Args:
        `vms` (list[VirtualMachine]): list of virtual machine to declare
        `dest_dir` (str): Destination root folder.
    """

    if not os.path.exists(dest_dir):
        log.info(f"Creating folder(s) {dest_dir}")
        os.makedirs(dest_dir)

    for vm in vms:
        generate_main_playbook(vm, dest_dir)
