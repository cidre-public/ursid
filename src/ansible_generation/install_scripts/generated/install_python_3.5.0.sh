#!/bin/bash
# it works
is_enough=$(python3 -c "import sys
current_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
against_version = '3.5.0'
against_version = tuple(map(int, against_version.split('.')))
print(int(against_version<=current_version))")
if [ "$is_enough" -eq 1 ]
then
  exit 0
fi
cd /tmp/
wget https://www.python.org/ftp/python/3.5.0/Python-3.5.0.tgz
tar -xzvf Python-3.5.0.tgz
cd Python-3.5.0/
sudo apt update
sudo -y apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev
./configure --enable-optimizations
make -j `nproc`
sudo make altinstall
temp=3.5.0
sudo ln -s -f /usr/local/bin/python"${temp%.*}" /usr/bin/python"${temp%.*}"
