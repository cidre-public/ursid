#!/bin/bash
export SUDO_FORCE_REMOVE=yes
apt purge sudo --assume-yes
wget https://www.sudo.ws/dist/sudo-1.8.27.tar.gz
tar -xvf sudo-1.8.27.tar.gz
cd sudo-1.8.27
./configure --with-secure-path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
make
make install
mv /usr/local/bin/sudo /usr/bin
chmod 4111 /usr/bin/sudo
cd ..
rm -r sudo-1.8.27tar.gz
rm -r sudo-1.8.27