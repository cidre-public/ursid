import os

from parsing.VM import VirtualMachine


def get_vm_playbook_name(vm: VirtualMachine) -> str:
    return f"main_playbook_{vm.name}.yml"
