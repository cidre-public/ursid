from virtualbox_interface.VNM import INTERNET_VAGRANT_IGNORE
import os

KEY_PATH = ".vagrant/machines/"
OUTPUT_PATH = "./output"


def post_launch_payload_detonation(vms):
    for vm in vms:
        if vm.postlaunch_commands:
            for command in vm.postlaunch_commands:
                machine_name = vm.name
                user_name = command["User"]
                str_command = command["Command"]
                prov_netw_ssh = ""
                for prov_netw in vm.get_provision_networks():
                    if not prov_netw == INTERNET_VAGRANT_IGNORE:
                        prov_netw_ssh = prov_netw
                        break
                machine_ip = vm.get_ip(prov_netw_ssh)
                key_path = f"""{KEY_PATH}{machine_name}/virtualbox/private_key"""
                bash_command = f"""ssh vagrant@{machine_ip} -i {key_path}
sudo -u {user_name} {str_command}"""
                with open(f"""{OUTPUT_PATH}/{machine_name}_{user_name}.sh """, "w+") as outFile:
                    outFile.write(bash_command)

