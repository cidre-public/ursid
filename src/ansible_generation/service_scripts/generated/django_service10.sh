#!/bin/bash
cd /home/bob/django_website_with_directory_traversal_rewarding_ssh_key
pip3 install pipenv
../.local/bin/pipenv install
../.local/bin/pipenv run python manage.py migrate
../.local/bin/pipenv run python3 manage.py loaddata ssh_key.json
../.local/bin/pipenv run python3 manage.py createsuperuser --noinput
ip4=$(/sbin/ip -o -4 addr list | grep 10.35 | awk '{print $4}' | cut -d/ -f1)
../.local/bin/pipenv run python manage.py runserver "$ip4":8000 &