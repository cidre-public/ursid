#!/usr/bin/env python3
import json
import shutil
import subprocess
import os, os.path
import logging
import argparse
import copy
from pathlib import Path
import utils.log, utils.user_confirm
from utils.static import static
from utils.colors import *
import parsing.parser as parser
import vagrant_generation.vagrant_generator as vg
import ansible_generation.ansible_generator as ag
import ansible_generation.post_launch_scripts as plscripts
import glob
from virtualbox_interface.VNM import delete_vboxnet

project_name = "Apiculteur"


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-y",
        "--auto-accept",
        action="store_true",
        help="Auto accept when being prompted to confirm some step",
    )
    parser.add_argument(
        "-d",
        "--destination",
        metavar="DESTINATION_DIR",
        default="output",
        type=str,
        help="Destination directory. It will be created if it does not exists.",
    )
    parser.add_argument(
        "SCENARIO_PATH",
        type=str,
        help="The path to the scenario",
        nargs="+"
    )
    parser.add_argument(
        "-vj",
        "--validate-json",
        action="store_true",
        help="Only validate the input scenario syntax. Stop after that.",
    )
    parser.add_argument(
        "--delete-vboxnets",
        action="store_true",
        help="Delete any existing vboxnets at launch. Good for testing. Bad if you have other VMs you care about.",
    )
    parser.add_argument(
        "--delete-previous-outputs",
        action="store_true",
        help="Delete the content of existing output folders at launch.",
    )

    parser.add_argument(
        "-aie",
        "--ansible-ignore-errors",
        action="store_true",
        help="Ansible will finish providing even if some tasks throw an error.",
    )
    parser.add_argument(
        "-nn",
        "--no-nat",
        action="store_true",
        help="NAT interface will be removed after the vms generation.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        help="Increase output verbosity",
    )

    return parser.parse_args()


def clean_target_dir(dest_dir: str):
    """Will clear a former Vagrant project by :
    * Running `vagrant destroy`
    * Deleting the old project files

    Args:
        dest_dir (str): target output directory.
    """
    log.warning(f'Running `vagrant destroy` inside "{dest_dir}" folder')
    cleaning = subprocess.Popen(
        "vagrant destroy --no-tty --force", cwd=dest_dir, shell=True
    )
    cleaning.wait()
    log.warning(
        f'Running `rm -Rf .vagrant Vagrantfile main_playbook_*` inside "{dest_dir}" folder'
    )
    cleaning = subprocess.Popen(
        "rm -Rf .vagrant Vagrantfile main_playbook_*", cwd=dest_dir, shell=True
    )
    cleaning.wait()


def verify_target_dir(dest_dir: str):
    """Will check if another vagrant project is running inside the
    target directory.
    If so it will propose to the user to run `vagrant destroy` inside
    this project before generating a new Vagrantfile.

    Args:
        dest_dir (str): target output directory.
    """
    if not os.path.exists(dest_dir):
        return
    if not os.path.exists(f"{dest_dir}/Vagrantfile"):
        return
    if not os.path.exists(f"{dest_dir}/.vagrant"):
        return
    log.warning(
        f"""There already is a Vagrant project that has been run before inside {dest_dir} folder.
It is recommended to perform a `vagrant destroy` command inside this project before anything."""
    )
    if utils.user_confirm.user_confirmation(
            f"Would you like {project_name} to clear the former project before generating a new one ?"
    ):
        clean_target_dir(dest_dir)
    else:
        exit(1)


def main(args):
    # Initial config
    if args.auto_accept:
        utils.user_confirm.ask_user = False

    if args.verbose:
        logging.info("Verbose output.")

    if args.validate_json:
        log.info("Verifying scenario syntax...")
        _, valid = parser.open_and_validate_json(args.SCENARIO_PATH)
        if valid:
            log.info("Trying a mock parse...")
            parser.parse_vms_from_json(args.SCENARIO_PATH)
            # If parse_vms_from_json didn't exit the program, then it has succeeded.
            log.info("Provided scenario presents a valid syntax.")
        else:
            log.error(
                "Provided scenario presents an error in the syntax. See above error messages."
            )
            log.error(
                "Please update your scenario accordingly. Otherwise please update the code and the schema.json."
            )
        return

    log.info("Sorting input list alphabetically...")
    args.SCENARIO_PATH.sort()
    log.info("Clearing generated service scripts folder")
    files = glob.glob('./ansible_generation/service_scripts/generated/*')
    for f in files:
        os.remove(f)

    log.info("Clearing generated install scripts folder")
    files = glob.glob('./ansible_generation/install_scripts/generated/*')
    for f in files:
        os.remove(f)

    vm_name_already_used = []
    # first check every target dir
    # has to be done before deleting all fbox nets, hence the copy pastes
    i = 0
    for scenario_path in args.SCENARIO_PATH:
        dest_dir = args.destination + "/scenario_" + str(i)
        verify_target_dir(dest_dir=dest_dir)
        i += 1
    # Remove previous networks
    # This had been missing for a while, and that combined with a previous bug had led to too many networks
    j = 0
    if args.delete_vboxnets:
        log.info("Deleting existing vboxnets...")
        delete_vboxnet()

    # always delete previous yamls
    files = glob.glob(args.destination + "/**/description.yml", recursive=True)
    log.info(f"Deleting {len(files)} previously generated yml...")
    for f in files:
        os.remove(f)

    if args.delete_previous_outputs:
        files = glob.glob(args.destination + "/*")
        if utils.user_confirm.user_confirmation(
                f"Directories {files} will be deleted and vagrant destroy will be ran, continue?"
        ):
            for f in files:
                clean_target_dir(f)
                shutil.rmtree(f)

    for scenario_path in args.SCENARIO_PATH:
        dest_dir = args.destination + "/scenario_" + str(j)
        # clear dest_dir
        j += 1

        log.info("Parsing scenario...")
        vms = parser.parse_vms_from_json(scenario_path, j, vm_name_already_used)

        log.info("Generating Vagrantfile...")
        vg.generate_vagrantfile(vms, dest_dir)

        log.info("Generating yaml...")
        vg.generate_yaml(vms, dest_dir)

        log.info("Generating Ansible playbooks...")
        ag.generate_all_main_playbooks(vms, dest_dir)

        log.info("Generating post launch scripts...")
        plscripts.post_launch_payload_detonation(vms)

        print(blue("\nApiculteur is done."))
        print(
            f"""
    To use the output you can run the following in your terminal :
    
    cd {dest_dir} && vagrant up
    """
        )


if __name__ == "__main__":
    args = parse_arguments()
    static["args"] = copy.deepcopy(args)
    utils.log.init_logging(args)
    log = logging.getLogger(project_name)
    log.debug(static)

    main(args)
