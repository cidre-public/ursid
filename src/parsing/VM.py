"""
Object representing a Virtual Machine
"""

import logging
import traceback
import json
import os.path

from virtualbox_interface.VNM import virtual_network_manager

log = logging.getLogger(__name__)


class VirtualMachine:
    def __init__(self):
        self.name: str = ""
        self.image: str = ""
        self.users: list[object] = []
        self.network_names: list[str] = []
        self.files: list[object] = []
        self.packages: list[object] = []
        self.repos: list[object] = []
        self.keys: list[object] = []
        self.inline_commands: list[str] = []
        self.inline_file_commands: list[object] = []
        self.postlaunch_commands: list[dict] = []
        self.used_ports = []
        # Self handled
        self.network_names_to_IP: dict = {}

    ##
    #
    # Setters/ Adders
    #
    ##

    def set_image(self, image: str):
        self.image = image

    def get_image(self):
        return self.image

    def add_port(self, port_string):
        self.used_ports.append(port_string)

    def add_file(
        self,
        dst: str,
        full_path_src: str,
        owner: str,
        group: str,
        perm: str,
        modif: str,
        before_packages: bool,
    ):
        """Add a file entry to the VM with the provided arguments.

        Args:
            * dst (str): destination on the guest
            * src (str): full path source on the host
            * owner (str): owner
            * group (str): group
            * perm (str): Permissions for the file (Unix format)
                * format is /[0-7]{4}/
                * (example : 0640)
            * before_packages (bool): Copy before packages or not
            * modif: modification to do to the file (edit/append/write)
        """
        self.files.append(
            {
                "Destination": dst,
                "Source": full_path_src,
                "Owner": owner.lower(),
                "Group": group.lower(),
                "Permissions": perm,
                "before_packages": before_packages,
                "Modification": modif
            }
        )

    def add_file_inline_command(self,
        dst: str,
        full_path_src: str,
        owner: str,
        group: str,
        perm: str,
        modif: str,
        before_packages: bool,
    ):
        self.inline_file_commands.append({
                "Destination": dst,
                "Source": full_path_src,
                "Owner": owner.lower(),
                "Group": group.lower(),
                "Permissions": perm,
                "before_packages": before_packages,
                "Modification": modif
            })

    def add_package(self, name: str, version: str, service_name: str = ""):
        """Add a package to the VM with the provided arguments.

        Args:
            * name (str): name of the package
            * version (str): version of the package, also use to know the type (.deb/url/...)
            * service_name (str) (default is null): name of the service associated to the package if exists
        """
        self.packages.append(
            {"name": name, "version": version, "service_name": service_name}
        )

    def add_repo(self, repo: str):
        """Add a repository to the VM with the provided arguments.

        Args:
            * repo (str): link of the repository
        """
        self.repos.append({"repo": repo})

    def add_keys(self, key_id: str, key_server: str):
        """Add a repo keys to the VM with the provided arguments.

        Args:
            * key_id (str): key of the repository
            * key_server (str): server where get the key
        """
        self.keys.append({"key_id": key_id, "key_server": key_server})

    def add_inline_command(self, inline: str):
        self.inline_commands.append(inline)

    def add_postlaunch_command(self, command: dict):
        self.postlaunch_commands.append(command)

    ##
    #
    # Getters
    #
    ##
    def get_users(self):
        return self.users

    def get_files(self) -> "list[object]":
        """Return files to be provisioned on the virtual machine

        Returns:
            list[object]: list of files object.

        Example:
            [
                {
                    "Destination": "/root/file1",
                    "Source": "/full/path/to/file1",
                    "Owner": "root",
                    "Group": "root",
                    "Permissions": "0640",
                    "before_packages": True
                }
            ]
        """
        return self.files

    def get_inline_file_commands(self):
        return self.inline_file_commands

    def get_provision_networks(self) -> "list[str]":
        # log.debug(self.network_names)
        return [
            virtual_network_manager.get_provision_network(network)
            for network in self.network_names
        ]

    def get_keys(self) -> "list[object]":
        """Return keys to be provisioned on the virtual machine

        Returns:
            list[object]: list of keys object.

        Example:
            [
                {
                    "key_id": "36A1D7869245C8950F966E92D8576A8BA88D21E9"
                    "key_server": "keyserver.ubuntu.com"
                }
            ]
        """
        return self.keys

    def get_repos(self) -> "list[object]":
        """Return repositories to be provisioned on the virtual machine

        Returns:
            list[object]: list of repositories object.

        Example:
            [
                {
                    "repo": "deb http://archive.canonical.com/ubuntu hardy partner"
                }
            ]
        """
        return self.repos

    def get_ip(self, provision_network) -> str:
        for network in self.network_names:
            if (
                virtual_network_manager.get_provision_network(network)
                == provision_network
            ):
                if network in self.network_names_to_IP.keys():
                    return self.network_names_to_IP[network]
                else:
                    ip_for_prov_network = (
                        virtual_network_manager.get_free_ip_address_for_network(
                            provision_network
                        )
                    )
                    self.network_names_to_IP[network] = ip_for_prov_network
                    return ip_for_prov_network
        log.error(
            f'Error getting IP address for VM "{self.name}" and provision_network "{provision_network}".'
        )
        exit(1)

    def get_packages(self) -> "list[object]":
        """Return packages to be provisioned on the virtual machine

        Returns:
            list[object]: list of packages object.

        Example:
            [
                {
                    "name": "foo"
                    "version": "latest"
                }
            ]
        """
        return self.packages

    def get_inline_commands(self) -> "list[str]":
        return self.inline_commands

    def get_postlaunch_commands(self):
        return self.postlaunch_commands

    def get_services_names(self) -> "list[str]":
        services = []
        for potential_service in self.get_packages():
            if potential_service["service_name"] != "":
                services.append(potential_service["service_name"])
        return services
