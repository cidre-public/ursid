#! /usr/bin/env python3
# coding: utf-8

"""
[EN]
This script will parse the JSON scenario file and generate the right configuration
-------------------------------------------------------------------------------------------------------------
Ce script va parser le fichier de scénario JSON et générer la configuration
[FR]
"""

import traceback
import json
import logging
import os, os.path
import re

from pyrsistent import v

from parsing.VM import VirtualMachine
from virtualbox_interface.VNM import virtual_network_manager as vnm
import parsing.validator as pv
import utils.text as text
import virtualbox_interface.manage as manage

log = logging.getLogger(__name__)
vm_name_number = 0
soft_cpe_data: dict = {}


def json_open(path) -> "object":
    if not os.path.exists(path):
        log.error(f"File not found: {path}")
        exit(1)
    try:
        j = json.load(open(path))
    except:
        log.error(f"Error loading json at {path}")
        log.error(f"Are you sure it is a json file ?")
        log.error(f"Run with -v option to have debug info.")
        log.debug(f"Exception faced: {traceback.format_exc()}")
        exit(1)
    return j


def open_and_validate_json(path: str) -> "tuple[list[dict], bool]":
    """Opens json at path.
    Verify its syntax.

    Args:
        `path` (str): path to the json file

    Returns:
        `(parsed_json, is_valid)`

        `is_valid == True` => the json corresponds to the schema

        `is_valid == False` => the json presented an error while parsing
    """
    scenario: list[dict] = json_open(path)
    if not pv.validate_json_scenario(scenario):
        is_valid = False
    else:
        is_valid = True
    return scenario, is_valid


def open_tweak_and_validate_json(path: str) -> "tuple[list[dict], bool]":
    # Tweaked version of the above function, checks the format but then creates a new json file
    # and opens that one instead
    scenario: list[dict] = json_open(path)
    if not pv.validate_json_scenario(scenario):
        is_valid = False
    else:
        is_valid = True
    scenario = scenario_format_conversion(scenario)
    return scenario, is_valid


def scenario_format_conversion(scenario_path, scenario_number, vm_names_already_used):
    # Changes the OS of all machine objects in a scenario to be CPEs
    with open(scenario_path, "r") as inFile:
        scenario = json.load(inFile)
    new_scenario = []
    for scenario_vm in scenario:
        if scenario_vm["Name"] in vm_names_already_used:
            i = 1
            while scenario_vm["Name"] + str(i) in vm_names_already_used:
                i += 1
            scenario_vm["Name"] = scenario_vm["Name"] + str(i)
            vm_names_already_used.append(scenario_vm["Name"])
        else:
            vm_names_already_used.append(scenario_vm["Name"])
        cpe_type = scenario_vm["OS"]["Type"]
        cpe_version = scenario_vm["OS"]["Version"]
        corresponding_cpe = "cpe:2.3:o:" + cpe_type + ":" + cpe_version + ":*"
        scenario_vm["OS"] = corresponding_cpe
        # Changes every user to a lower case name
        for i in range(len(scenario_vm["Users"])):
            user = scenario_vm["Users"][i]
            scenario_vm["Users"][i]["Name"] = user["Name"].lower()
            scenario_vm["Users"][i]["Group"] = user["Group"].lower()
        # make them all part of the same network if no net
        if "Networks" not in scenario_vm:
            scenario_vm["Networks"] = ["Default" + str(scenario_number)]
        # change every file owner/group to a lower case name
        for i in range(len(scenario_vm["Files"])):
            file = scenario_vm["Files"][i]
            scenario_vm["Files"][i]["Owner"] = file["Owner"].lower()
            scenario_vm["Files"][i]["Group"] = file["Group"].lower()
        # change every software (vendor: product) to product only
        for i in range(len(scenario_vm["Software"])):
            software = scenario_vm["Software"][i]
            split_list = software["Type"].split(":")
            if len(split_list) == 1:  # it wasn't a cpe in the first place:
                scenario_vm["Software"][i]["Type"] = software["Type"].split(":")[0]
            else:  # it was a cpe
                scenario_vm["Software"][i]["Type"] = software["Type"].split(":")[1]

        new_scenario.append(scenario_vm)
    output_path = os.path.dirname(scenario_path) + "/converted_" + os.path.basename(os.path.splitext(scenario_path)[0]) + ".json"
    with open(output_path, "w+") as outFile:
        json.dump(new_scenario, outFile)
    return output_path


def parse_vms_from_json(path_to_scenario: str, scenario_number: int, vm_names_already_used: list[str]):
    # scenario_number is useful to create new network names for each scenario
    # hopefully this will lead to the VNM creating automatically new IPs
    # scenario: list[dict] = json_open(path_to_scenario)
    # vm_names_already_used is a list of vm names that if encountered need to be changed
    # for instance when deploying the same scenario twice, if machine zagreus was already generated the second one will
    # be named zagreus2
    scenario, is_valid = open_and_validate_json(path_to_scenario)
    if not is_valid:
        log.error("Aborting due to precedent error.")
        exit(1)
    path_to_scenario = scenario_format_conversion(path_to_scenario, scenario_number, vm_names_already_used)
    vms: list[VirtualMachine] = []
    scenario = json_open(path_to_scenario)
    for vm_object in scenario:
        vm = VirtualMachine()
        parse_vm(vm, vm_object, path_to_scenario)
        # Check vm name format
        check_vm_name_format(vm.name)
        # Check if vm name is already use by virtualbox
        vm.name = manage.check_vm_name(vm.name)

        vms.append(vm)
        log.debug(f"{vm.name} vm parsed")
    # Check vm unicity in the json file
    check_vm_name_unicity(vms)
    return vms


def check_vm_name_unicity(vms: "list[VirtualMachine]"):
    """
    Check vms name unicity and correct if needed

    Args:
        `vms` (list[VirtualMachine]): list of virtual machine to check
    """
    for vm_to_check in vms:
        for vm in vms:
            if vm_to_check != vm and vm.name == vm_to_check.name:
                global vm_name_number
                vm_name_number += 1
                vm.name = vm.name + str(vm_name_number)
                log.info(f"Two vms have the same name, suffix added to one of them.")


def check_vm_name_format(name: str):
    """
    Check vms name format. Errors and Exits if it does not match.

    Args:
        `name` (str): Name of the vm
    """
    name_format = "^[a-zA-Z0-9]+$"
    if not bool(re.match(name_format, name)):
        log.error(f"{name} don't match vm name requirements.")
        log.error("Name field can only contain alpha-numeric values.")
        exit(1)


def parse_vm(vm: VirtualMachine, vm_object: object, path_to_scenario: str):
    """Parses the vm_object
    Args:
        vm (VirtualMachine): VirtualMachine object to complete
        vm_object (object): Contains all the information for the VM to be provided and provisioned.
        It should have been validated by parsing.validator functions first.
        path_to_scenario (str): path to the scenario JSON file.
        It is used in order to verify the existences of the files that are to be copied by the provisioner
        onto the generated virtual machines.
    """
    global soft_cpe_data
    soft_cpe_data = json_open(
        os.path.join(os.path.dirname(__file__), "../data/software-cpe.json")
    )
    try:
        # string in lowercase (lowercase is a requirement from Vagrantfiles for vm names)
        vm.name = vm_object["Name"].lower()
        use_image = 0
        if "Image" in vm_object.keys() and "OS" in vm_object.keys():
            use_image = True

        else:
            use_image = False
        if "Image" in vm_object.keys() or use_image == True:
            if "OS" in vm_object.keys():
                log.warning(
                    'Both "Image" and "OS" are defined in the scenario. The "Image" parameter will be used. The "OS" parameter will be ignored.'
                )
            vm.set_image(vm_object["Image"])
        elif "OS" in vm_object.keys():
            # Condition means parse_os failed.
            if not parse_os(vm, vm_object["OS"]):
                log.error(
                    f"Failed parsing OS-CPE (\"{vm_object['OS-CPE']}\") name for \"{vm.name}\"."
                )
                log.error(
                    f'This might be a bug in the "parser.parse_os" function. Or the image you are targeting might be missing from "data/images.json".'
                )
                exit(1)
        else:
            log.error(
                f'Please define "Image" or "OS" keys for VM: "{vm.name}" in your scenarios.'
            )
            exit(1)

        NAME_REGEX = "^[a-z][-a-z0-9]*$"
        failed = False
        for user in vm_object["Users"]:
            if not bool(re.match(NAME_REGEX, user["Name"])):
                failed = True
                log.error(
                    f'Failed parsing user "{user["Name"]}" from VM "{vm.name}". It did not match NAME_REGEX=/{NAME_REGEX}/.'
                )
        for user in vm_object["Users"]:
            if [usr["Name"] for usr in vm_object["Users"]].count(user["Name"]) > 1:
                failed = True
                log.error(
                    f'Failed parsing user "{user["Name"]}" from VM "{vm.name}". It was found more than once in the user list.'
                )

        if failed:
            log.error("Exiting due to previous error(s).")
            exit(1)

        # list[(username, password), ...]
        for user in vm_object["Users"]:
            if "Services" in user:

                vm.users = [{"Name": user["Name"],
                             "Group": user["Group"],
                             "Credentials": user["Credentials"],
                             "Services": user["Services"],
                             "Privilege": user["Privilege"]}
                            for user in vm_object["Users"]]
            else:
                vm.users = [{"Name": user["Name"],
                             "Group": user["Group"],
                             "Credentials": user["Credentials"],
                             "Privilege": user["Privilege"]}
                            for user in vm_object["Users"]]

        # Networks
        parse_networks(vm, vm_object["Networks"])

        # Packages
        if "Software" in vm_object.keys():
            # Config Files of packages
            for software in vm_object["Software"]:
                if "Config" in software.keys() and type(software["Config"]) == dict:
                    if "Files" in software["Config"].keys():
                        log.warning(
                            "Files in Software.Config are not parsed. Move them to VM.Files."
                        )
                if "Deb" in software.keys() and type(software["Deb"]) == str:
                    # If the field is not a link, we consider it is a file on the host
                    if software["Deb"][:4] != "http":
                        deb_file = [
                            {
                                "Destination": "/tmp/",
                                "Source": software["Deb"],
                                "Owner": "root",
                                "Group": "root",
                                "Permissions": "0644",
                            }
                        ]
                        parse_files(vm, deb_file, path_to_scenario, True)
                parse_software(vm, software)

        # Files
        if "Files" in vm_object.keys():
            parse_files(vm, vm_object["Files"], path_to_scenario)

        # Inline commands
        if "Inline" in vm_object.keys():
            for inline in vm_object["Inline"]:
                vm.add_inline_command(inline)

        if "Inline Files" in vm_object.keys():
            parse_file_inline_commands(vm, vm_object["Inline Files"], path_to_scenario)

        if "Postlaunch commands" in vm_object.keys():
            for postlaunch in vm_object["Postlaunch commands"]:
                vm.add_postlaunch_command(postlaunch)

    except Exception as e:
        log.error("Error parsing VirtualMachine json:")
        log.error("Please verify its correctness,")
        log.error(json.dumps(vm_object, indent=4))
        log.error(f"Exception faced: {traceback.format_exc()}")
        exit(1)


def parse_os(vm: VirtualMachine, os_cpe: str) -> bool:
    if text.is_cpe(os_cpe):
        cpe = text.parse_cpe(os_cpe)

        images_data_path = os.path.join(
            os.path.dirname(__file__), "../data/images.json"
        )
        log.debug(f"Parsing cpe: {cpe}")
        img_data = json_open(images_data_path)

        log.debug(f"Trying distribs {img_data}")
        for distrib in img_data.keys():
            log.debug(f"Trying distrib {distrib}")
            if re.match(distrib, cpe["product"]):
                log.debug(f"CPE matches Distrib")
                # Looking for a {distrin} OS:
                target = cpe["version"]
                closest = text.find_closest_version(
                    target, list(img_data[distrib].keys())
                )
                log.debug(f"Target: {target}; Closest: {closest};")
                if closest == False:
                    log.error(
                        f'Error looking for closest version of "{target}" in "{img_data[distrib].keys()}"'
                    )
                    exit(1)
                # chosen_version = (available_versions[0][0], available_versions[0][1])
                # for str_v, v in available_versions:
                #     if abs(target_version - v) < abs(
                #         target_version - chosen_version[1]
                #     ):
                #         chosen_version = str_v, v
                target_p = text.parse_version(target)
                closest_p = text.parse_version(closest)

                if (
                        target_p[0] != closest_p[0]
                        or target_p[1] != closest_p[1]
                        or target_p[2] != closest_p[2]
                ):
                    log.warning(
                        f"""There is a gap between the chosen img OS and the wanted OS:
The image that was selected for "{vm.name}" of wanted OS: "{os_cpe}" is "{img_data[distrib][closest]}"(v{closest})."""
                    )
                vm.set_image(img_data[distrib][closest])
                return True
    return False


def parse_networks(vm: VirtualMachine, network_names: "list[str]"):
    vm.network_names = network_names
    for network in network_names:
        vnm.maybe_add_network(network)


def parse_files(
        vm: VirtualMachine,
        files: "list[object]",
        path_to_scenario: str,
        before_packages: bool = False,
):
    base_scenario_path = os.path.dirname(path_to_scenario)
    perm_pattern = "[0-7]{4}"
    for file in files:
        file_full_path = os.path.join(os.getcwd(), base_scenario_path, file["Source"])
        log.debug(f"Parsing file path {file_full_path}")
        if not os.path.exists(file_full_path):
            log.error(f"Error while parsing {vm.name}:")
            log.error(
                f"{file_full_path} was not found. Please make sure files all files are provided next to the scenario."
            )
            exit(1)
        elif not re.match(perm_pattern, file["Permissions"]):
            log.error(f"Error while parsing {vm.name}:")
            log.error(
                f'Permission format expected something similar to 0640 (matching /{perm_pattern}/) but found "{file["Permissions"]}" instead.'
            )
            exit(1)
        else:
            dst = file["Destination"]

            if "~/" == file["Destination"][0:2]:
                dst = os.path.join(f"/home/{file['Owner']}", file["Destination"][2:])
            vm.add_file(
                dst=dst,
                full_path_src=file_full_path,
                owner=file["Owner"],
                group=file["Group"],
                perm=file["Permissions"],
                before_packages=before_packages,
                modif=file["Modification"]
            )


def parse_file_inline_commands(
        vm: VirtualMachine,
        files: "list[object]",
        path_to_scenario: str,
        before_packages: bool = False,
):
    # Very ugly copy paste of the file command
    base_scenario_path = os.path.dirname(path_to_scenario)
    perm_pattern = "[0-7]{4}"
    for file in files:
        file_full_path = os.path.join(os.getcwd(), base_scenario_path, file["Source"])
        log.debug(f"Parsing file path {file_full_path}")
        if not os.path.exists(file_full_path):
            log.error(f"Error while parsing {vm.name}:")
            log.error(
                f"{file_full_path} was not found. Please make sure files all files are provided next to the scenario."
            )
            exit(1)
        elif not re.match(perm_pattern, file["Permissions"]):
            log.error(f"Error while parsing {vm.name}:")
            log.error(
                f'Permission format expected something similar to 0640 (matching /{perm_pattern}/) but found "{file["Permissions"]}" instead.'
            )
            exit(1)
        else:
            dst = file["Destination"]
            if "~/" == file["Destination"][0:2]:
                dst = os.path.join(f"/home/{file['Owner']}", file["Destination"][2:])
            vm.add_file_inline_command(
                dst=dst,
                full_path_src=file_full_path,
                owner=file["Owner"],
                group=file["Group"],
                perm=file["Permissions"],
                before_packages=before_packages,
                modif=file["Modification"]
            )


def parse_software(
        vm: VirtualMachine,
        software: dict,
        called_from_cpe: bool = False,
):
    global soft_cpe_data

    if "Port" in software.keys():
        vm.add_port(software["Port"])

    potential_service_name = ""
    if "Config" in software.keys():
        if "ServiceName" in software["Config"].keys():
            potential_service_name = software["Config"]["ServiceName"]

    # .deb file case
    if "Deb" in software.keys():
        # If the field is a link
        if software["Deb"][:4] == "http":
            name = software["Deb"]
        # else, it should be a file path on host
        else:
            # we consider that the file will be in /tmp
            name = "/tmp/" + software["Deb"].split("/")[-1]
        version = "deb"

    # cpe case
    elif "CPE" in software.keys():
        if called_from_cpe:
            log.error(
                f'Infinite loop detected while parsing CPE "{software["CPE"]}". Please do not use any CPE in data/software-cpe.json'
            )
            exit(1)
        if len(soft_cpe_data.keys()) == 0:
            log.warning(
                f'Failed trying to parse CPE "{software["CPE"]}" but nothing was loaded from data/software-cpe.json'
            )

        # cpe parsing
        cpe_package = text.parse_cpe((software["CPE"]))
        vendor = cpe_package["vendor"]
        name = cpe_package["product"]
        version = cpe_package["version"]
        if vendor in soft_cpe_data.keys():
            if name in soft_cpe_data[vendor].keys():
                if version in soft_cpe_data[vendor][name].keys():
                    parse_software(
                        vm, soft_cpe_data[vendor][name][version], called_from_cpe=True
                    )
                    return
                # The filter(lambada ...) is used to filter out the "*" keys.
                elif (
                        closest := text.find_closest_version(
                            version,
                            [
                                _
                                for _ in filter(
                                lambda v: v != "*", soft_cpe_data[vendor][name]
                            )
                            ],
                        )
                ):
                    parse_software(
                        vm, soft_cpe_data[vendor][name][closest], called_from_cpe=True
                    )
                    return
                elif "*" in soft_cpe_data[vendor][name].keys():
                    parse_software(
                        vm, soft_cpe_data[vendor[name]["*"]], called_from_cpe=True
                    )
                    return

    # other cases, in repository, default or not
    else:
        name = software["Type"]
        # If repo is mentionned, we add it
        if "Repos" in software:
            for package_repo in software["Repos"]:
                repo = package_repo["repo"]
                # If key id and key server are mentionned, we add these
                if "key_id" in package_repo and "key_server" in package_repo:
                    vm.add_repo(repo=repo)
                    vm.add_keys(
                        key_id=package_repo["key_id"],
                        key_server=package_repo["key_server"],
                    )
                # If only repo is mentionned, we add it
                else:
                    vm.add_repo(repo=repo)
                log.debug(f"Repository {repo} parsed")

        # If version is not specified
        if "Version" not in software:
            version = "latest"
        # If version is specified
        else:
            version = software["Version"]

    log.debug(f"Parsing package {name} in {vm.name}")
    # Finally add the package
    vm.add_package(name=name, version=version, service_name=potential_service_name)
