#! /usr/bin/env python3
# coding: utf-8

"""
[EN]
This script will validate the scheme and types of the JSON scenario
-------------------------------------------------------------------------------------------------------------
Ce script va valider le schéma et le typage du scénario JSON
[FR]

pip install jsonschema
"""

import json
import jsonschema, jsonschema.exceptions
import logging
import os.path

log = logging.getLogger(__name__)

# Importing the schema we want to validate
with open(f"{os.path.dirname(__file__)}/new_schema.json", "r") as f:
    scenario_schema = json.load(f)
    f.close()


def validate_json_scenario(j):
    try:
        jsonschema.validate(instance=j, schema=scenario_schema)
    except jsonschema.exceptions.ValidationError as err:
        log.debug(err)
        log.error(
            f"""            
Error while looking for required {err.validator_value} inside the scenario.
{err.message} :
{json.dumps(err.instance, indent=4)}"""
        )
        log.warning(
            'Note that some fields can be replaced by others: you can put either "Image" or "OS".'
        )
        log.warning(
            'Please check the "schema.json" in order to better understand the scenario foramt.'
        )
        return False
    return True
