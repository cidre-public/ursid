cd ../scenarios/cerbere
FILES=( $(find . -type f -name "output_scenario.json") )
echo "${FILES[*]}"
echo "${#FILES[@]}"
REAL_FILES=( $(for f in ${FILES[*]}; do realpath "$f"; done) )
cd ../../src
echo "${#REAL_FILES[@]}"
python3 main.py "${REAL_FILES[@]}" -y --delete-vboxnets --delete-previous-outputs
cd output
for ((i=0; i< ${#REAL_FILES[@]}; i++ ));
do
  cd ./scenario_"$i" || exit
  vagrant up
  cd ..
done
