import os
import sys


def color(text: str, color_code: int) -> str:
    """
    Returns the colored text for an UNIX terminal.
    It will do nothing on a windows computer

    Args:
        `text` (str): [description]
        `color_code` (int): [description]

    Returns:
        str: the colored text.
    """
    if sys.platform == "win32":
        return text
    return f"\x1b[{color_code}m{text}\x1b[0m"


def red(text):
    return color(text, 31)


def green(text):
    return color(text, 32)


def yellow(text):
    return color(text, 33)


def blue(text):
    return color(text, 34)


def magenta(text):
    return color(text, 35)
