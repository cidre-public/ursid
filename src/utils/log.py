import logging
import copy
from utils.colors import *


project_title_unicode = """
             _            _ _                  
            (_)          | | |                 
  __ _ _ __  _  ___ _   _| | |_ ___ _   _ _ __ 
 / _` | '_ \| |/ __| | | | | __/ _ \ | | | '__|
| (_| | |_) | | (__| |_| | | ||  __/ |_| | |   
 \__,_| .__/|_|\___|\__,_|_|\__\___|\__,_|_|   
      | |                                      
      |_|
"""


class DefaultConsoleHandler(logging.StreamHandler):
    def emit(self, record: logging.LogRecord):
        colored = copy.copy(record)

        if record.levelname == "WARNING":
            colored.msg = yellow(record.msg)
            colored.levelname = yellow(record.levelname)
        elif record.levelname == "CRITICAL" or record.levelname == "ERROR":
            colored.msg = red(record.msg)
            colored.levelname = red(record.levelname)
        elif record.levelname == "DEBUG":
            colored.msg = magenta(record.msg)
            colored.levelname = magenta(record.levelname)
        else:
            colored.msg = record.msg

        logging.StreamHandler.emit(self, colored)


def init_logging(args):
    log_format = "[%(asctime)s] %(levelname)-8s %(name)-12s %(message)s"
    if args.verbose:
        logging.basicConfig(
            level=logging.DEBUG,
            format=log_format,
            handlers=[DefaultConsoleHandler()],
            datefmt="%d/%m/%Y %I:%M:%S",
        )
    else:
        logging.basicConfig(
            level=logging.INFO,
            format=log_format,
            handlers=[DefaultConsoleHandler()],
            datefmt="%d/%m/%Y %I:%M:%S",
        )
    print(yellow(project_title_unicode))
