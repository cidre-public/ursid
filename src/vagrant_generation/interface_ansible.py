from parsing.VM import VirtualMachine
import ansible_generation.interface_vagrant as int_vagrant


def ansible_provisioning_vm(vm: VirtualMachine):
    return f"""
{vm.name}.vm.provision "ansible" do |ansible|
    ansible.playbook = "{int_vagrant.get_vm_playbook_name(vm)}"      
end 
"""


def get_playbook_path(vm: VirtualMachine):
    return int_vagrant.get_vm_playbook_name(vm)
