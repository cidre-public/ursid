from io import TextIOWrapper
import logging
import os, os.path

from parsing.VM import VirtualMachine
from virtualbox_interface.VNM import INTERNET_VAGRANT_IGNORE
from utils.static import static
import utils.text as text
import vagrant_generation.interface_ansible as interface_ansible
import re

log = logging.getLogger(__name__)


def generate_vagrantfile(vms: "list[VirtualMachine]", dest_dir: str):
    """
    Generate a Vagrantfile from a list of virtual machine objects

    Args:
        `vms` (list[VirtualMachine]): list of virtual machine to declare
        `dest_dir` (str): Destination root folder.
    """

    if not os.path.exists(dest_dir):
        log.info(f"Creating folder(s) {dest_dir}")
        os.makedirs(dest_dir)

    vagrantfile_path = dest_dir + "/Vagrantfile"
    vagrantfile = open(vagrantfile_path, "w")
    vagrantfile.write(
        f"""# -*- mode: ruby -*-
# vi: set ft=ruby :
DIR_NAME = File.basename(Dir.getwd)
# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

Vagrant.configure("2") do |config|
    config.vm.synced_folder ".", "/vagrant", disabled: true
"""
    )
    for vm in vms:
        vagrantfile.write(text.indent_str(vm_to_vagrant(vm, dest_dir), 4))

    vagrantfile.write(
        """
end
"""
    )

    log.debug("Vagrantfile generated")


def configure_networks(vm: VirtualMachine) -> str:
    networks_vagrant_conf = ""
    for prov_netw in vm.get_provision_networks():
        if not prov_netw == INTERNET_VAGRANT_IGNORE:
            networks_vagrant_conf += f'{vm.name}.vm.network :private_network, ip: "{vm.get_ip(prov_netw)}", name: "{prov_netw}"\n'
    return networks_vagrant_conf


def command_to_vagrant(vm: VirtualMachine, command: str, msg: str):
    return f"""
    {vm.name}.trigger.after :up, :provision do |trigger|
        trigger.info = "{msg}"
        trigger.run = {{inline: "{command}"}}
    end
    """


def generate_yaml(vms: "list[VirtualMachine]", dest_dir: str) -> str:
    for vm in vms:
        yaml_path = dest_dir + "/description.yml"
        with open(yaml_path, "a+") as outFile:
            outFile.write("- name: " + vm.name + "\n")
            outFile.write("  box: " + vm.image + "\n")
            for prov_netw in vm.get_provision_networks():
                if not prov_netw == INTERNET_VAGRANT_IGNORE:
                    vm_ip = vm.get_ip(prov_netw)  # TODO: fix for more than one network
            # convert vboxnet to cerberenet
            m = re.search(r'\d+$', prov_netw)
            m = int(m.group())
            outFile.write("  network: " + "cerberenet" + str(m) + "\n")
            # look for port in vm software
            if vm.used_ports:
                outFile.write("  web_port: " + vm.used_ports[0] + "\n")
            outFile.write("  playbook: " + interface_ansible.get_playbook_path(vm) + "\n")
            outFile.write("  group: " + "cerbere" + "\n")


def vm_to_vagrant(vm: VirtualMachine, dest_dir: str) -> str:
    print_ip_cmd = 'echo \\"Printing IP addresses:\\" && hostname -I|| echo \\"An error has occured while printing IP addresses\\"'
    prov_netw_ssh = ""
    for prov_netw in vm.get_provision_networks():
        if not prov_netw == INTERNET_VAGRANT_IGNORE:
            prov_netw_ssh = prov_netw
            break

    ssh_connection_cmd = f"ssh vagrant@{vm.get_ip(prov_netw_ssh)} -i {os.path.abspath(dest_dir)}/.vagrant/machines/{vm.name}/virtualbox/private_key"

    end = f"""
config.vm.define :{vm.name} do |{vm.name}|
    {vm.name}.vm.box = "{vm.image}"
    {vm.name}.vm.hostname = "{vm.name}"

    {vm.name}.vm.provider :virtualbox do |vb|
      vb.name = "{vm.name}"
    end
{text.indent_str(interface_ansible.ansible_provisioning_vm(vm))}
{text.indent_str(configure_networks(vm))}
"""
    if static["args"].no_nat:
        end += command_to_vagrant(
            vm,
            f"vboxmanage controlvm {vm.name} poweroff",
            "Remove NAT interface : turn the vm off",
        )
        end += command_to_vagrant(
            vm,
            f"vboxmanage modifyvm {vm.name} --nic1=null",
            "Remove NAT interface",
        )
        end += command_to_vagrant(
            vm,
            f"vboxmanage startvm {vm.name} --type=headless",
            "Remove NAT interface : turn the vm on",
        )
    else:
        end += command_to_vagrant(
            vm,
            f"bash vagrant ssh {vm.name} -c '{print_ip_cmd}'",
            "IP addresses",
        )
    end += command_to_vagrant(
        vm,
        f"""echo \\"SSH Command:\\n\\n\\" '{ssh_connection_cmd}'""",
        "Printing ssh command",
    )
    end += """
end
"""
    return end
