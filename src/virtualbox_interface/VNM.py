"""
[EN]
This script allows the project to list the networks necessary to later interact 
with VirtualBox to list and create missing virtual networks.  
-------------------------------------------------------------------------------------------------------------
Ce script permet au projet de lister les réseaux nécessaires pour ensuite intéragir
avec VirtualBox pour lister et créer les réseaux virtuels nécessaires.
[FR]
"""

import logging
import utils.command as u
import ipaddress

log = logging.getLogger(__name__)

# Arbitrary set to 20, no technical reason behind this.
MAX_NETWORK_COUNT = 20

INTERNET_VAGRANT_IGNORE = "INTERNET_VAGRANT_IGNORE"


def catch_network_list() -> "list[dict[('name', str),('ip_addr', str)]]":
    """
    Get the network list in a array form.
    This is done by asking virtualbox directly which networks it has available and parsing the result.

    Returns:
        array:   `command_output`    ->    networks

    Example output:
        [
            {
                "name":"vboxnet0"
                "ip_addr":ipaddress("192.168.56.1")
            },
            {
                "name":"vboxnet2"
                "ip_addr":ipaddress("192.168.58.1")
            },
        ]
    """
    output: str = u.commande("vboxmanage list hostonlyifs")
    # Parsing the command output.
    if len(output) == 0:
        return []

    networks = []
    for unparsed_nw in output.strip().split("\n\n"):
        nw = {}
        for unparsed_param in unparsed_nw.split("\n"):
            # log.debug(
            #     f"Catch_network_list : param pairs are {unparsed_param.split(': ')}"
            # )
            [key, value] = unparsed_param.split(": ")
            if key.strip() == "Name":
                nw["name"] = value.strip()
            elif key.strip() == "IPAddress":
                nw["ip_addr"] = value.strip()
        networks.append(nw)

    # log.debug(networks)
    return networks


def create_vboxnet(number: int = 1):
    """
    Create numbers of vboxnet we want

    Args:
        int:     `number`            ->    number of networks to create
    """
    # We create the required number of networks
    for i in range(number):
        u.commande("vboxmanage hostonlyif create")

    # Log of the number of created networks
    # log.debug(str(number) + " networks were created")


def delete_vboxnet():
    """
    Delete all vboxnets
    """
    # We get the number of networks
    nbNetwork = len(catch_network_list())
    # We create a decreasing loop from the number of networks-1 to 0
    # Exemple with 3 networks: vboxnet2 - vboxnet1 - vboxnet0
    for i in range(nbNetwork - 1, -1, -1):
        u.commande("vboxmanage hostonlyif remove vboxnet" + str(i))

    # Log of the number of deleted networks
    log.info(str(nbNetwork) + " networks were removed.")


class VirtualNetworkManager:
    """This class uses VBoxManage logic and assumes that virtualbox networks
    are going to be used. If virtualbox stopped behing part of the provider,
    this class must be updated.
    """

    def __init__(self):
        # {"network_name": "network_provision"}
        self.known_networks = {"Internet": INTERNET_VAGRANT_IGNORE}
        # {"network_provision" : ipaddress}
        self.free_ips: dict[(str, ipaddress.IPv4Address)] = {}
        self.network_count = 0
        self.virtual_provision_networks_created = False

    def maybe_add_network(self, network_name: str):
        self.virtual_provision_networks_created = False
        if network_name in self.known_networks.keys():
            return
        elif self.network_count >= MAX_NETWORK_COUNT:
            log.error(f"Maximum network count ({MAX_NETWORK_COUNT}) reached.")
            exit(1)
        else:
            self.known_networks[network_name] = f"vboxnet{self.network_count}"
            self.network_count += 1

    def create_virtual_provision_networks(self):
        log.debug("Verifying availability of virtual networks...")

        # Step 1: count the amount of missing networks
        virtual_networks_available = catch_network_list()
        virtual_networks_available_names = [
            n["name"] for n in virtual_networks_available
        ]

        amount_of_missing_networks = 0
        for network in self.known_networks.values():
            if (
                not network in virtual_networks_available_names
                and not network == INTERNET_VAGRANT_IGNORE
            ):
                log.debug(f"Network {network} is not available.")
                amount_of_missing_networks += 1

        # Step 2: create the necessary amount of virtual networks
        if amount_of_missing_networks > 0:
            log.debug(f"Creating {amount_of_missing_networks} virtual networks...")
            create_vboxnet(amount_of_missing_networks)

        # Step 3: verify that all virtual networks are availalbe as they should be
        virtual_networks_available = catch_network_list()
        virtual_networks_available_names = [
            n["name"] for n in virtual_networks_available
        ]

        for network in self.known_networks.values():
            if (
                not network in virtual_networks_available_names
                and not network == INTERNET_VAGRANT_IGNORE
            ):
                log.error(
                    f"Network {network} still not available after creating {amount_of_missing_networks}."
                )
                log.error("This is a bug.")
                exit(1)
        self.virtual_provision_networks_created = True
        return

    def get_provision_network(self, network_name) -> str:
        if not self.virtual_provision_networks_created:
            self.create_virtual_provision_networks()

            # Fill next ip_addresses dict
            provision_networks_list = catch_network_list()
            for nw in provision_networks_list:
                # + 1 because the returned ip address is the one taken by the host
                self.free_ips[nw["name"]] = ipaddress.ip_address(nw["ip_addr"]) + 1

        if not network_name in self.known_networks.keys():
            log.error(
                f'Tried to get the network_provision of an unkown network: "{network_name}"'
            )
            log.error(
                f"This bug should not happen. Please make sure the parser adds all the networks to the static VirtualNetworkManager."
            )
            exit(1)
        return self.known_networks[network_name]

    def get_free_ip_address_for_network(self, prov_network) -> ipaddress.IPv4Address:
        """
        Takes the name of a provision network as an argument.

        Example: "vboxnet0"
        """
        if prov_network == INTERNET_VAGRANT_IGNORE:
            log.error(
                f"Can't give an IP for the Internet network that should be ignored by Vagrant."
            )
            exit(1)

        free_ip = self.free_ips[prov_network]
        self.free_ips[prov_network] += 1
        return free_ip


# Static instance. No other instance should be needed at the time implementing this.
virtual_network_manager = VirtualNetworkManager()
