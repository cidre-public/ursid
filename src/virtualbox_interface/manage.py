import logging
import utils.command as c

log = logging.getLogger(__name__)
vm_name_number = 0


def check_vm_name(vmname: str):
    """
    List VMs and check if a vm already exist with the given name
    """
    vms = c.command_complex(f"vboxmanage list vms")
    for vm in vms.split("\n"):
        name = vm.split(" ")[0][1:-1]
        if name == vmname:
            global vm_name_number
            vm_name_number += 1
            vmname += str(vm_name_number)
            log.warning(
                f"{name} name already use by virtualbox, suffix added in the non generated one."
            )
            log.warning(f"new name : {vmname}")
    return vmname


def stop_vm(vmname: str):
    """
    Stop VM

    Args:
        string:   `vmname`            ->    name of the VM
    """
    c.command_complex(f"vboxmanage controlvm {vmname} poweroff")
    log.debug(f"{vmname} VM halted.")


def start_vm(vmname: str):
    """
    Start VM

    Args:
        string:   `vmname`            ->    name of the VM
    """
    c.command_complex(f"vboxmanage startvm {vmname} --type=headless")
    log.debug(f"{vmname} VM started.")


def delete_nat(vmname: str):
    """
    Delete nat network

    Args:
        string:   `vmname`            ->    name of the VM
    """
    c.command_complex(f"vboxmanage modifyvm {vmname} --nic1=null")
    log.info(f"NAT network removed from {vmname}.")
